# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic.base import View

from django.shortcuts import render, HttpResponseRedirect,get_object_or_404

from django.contrib.sessions.models import Session

from django.contrib import auth
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator
from django.urls import reverse

import datetime

from feedback import forms
from feedback import models
from feedback.utils import add_list_page, count_new_application, add_nokey_session

# view -1
ROWS_PAGE = 5


class Base(View):

    def get(self, request):
        request =add_nokey_session(request)
        form_people = forms.PeopleForm()
        form_car = forms.CarForm()
        form_application = forms.ApplicationForm()
        form_file_one = forms.CarMediaForm()
        context = {
            'form_people': form_people,
            'form_car': form_car,
            'form_application': form_application,
            'form_car_media_one': form_file_one,
        }
        return render(request, 'feedback/base.html', context)

    def post(self, request, *args, **kwargs):
        post_form_people = forms.PeopleForm(request.POST)
        post_form_car = forms.CarForm(request.POST)
        post_form_application = forms.ApplicationForm(request.POST)
        post_form_file = forms.CarMediaForm(request.POST, request.FILES)

        if post_form_people.is_valid() and post_form_application.is_valid() \
                and post_form_car.is_valid() and post_form_file.is_valid():

            buf_people = None

            if models.People.objects.filter(phone=request.POST.get("phone")).exists():
                buf_people = models.People.objects.get(phone=request.POST.get("phone"))
            else:
                buf_people = post_form_people.save()

            new_car = post_form_car.save(commit=False)
            new_car.people = buf_people

            buf_car = post_form_car.save()

            new_file = post_form_file.save(commit=False)
            new_file.car = buf_car
            post_form_file.save()

            new_application = post_form_application.save(commit=False)
            new_application.car = buf_car
            new_application.people = buf_people
            new_application.date = datetime.datetime.now()

            post_form_application.save()

            return HttpResponseRedirect("/")

        context = {
            'form_people': post_form_people,
            'form_car': post_form_car,
            'form_application': post_form_application
        }
        return HttpResponseRedirect("/", context)


class OneApplication(View):

    def get(self, request, *args, **kwargs):
        buf_application = get_object_or_404(models.Application, id=kwargs.get('application_id'))
        buf_application.viewed = True
        buf_application.save()
        medial_url = []
        all_media = models.CarMedia.objects.filter(car=buf_application.car)
        if len(all_media) > 0:
            for one_media in all_media:
                if one_media.file != '':
                    medial_url.append(unicode(str(one_media.file.url)))
        context = {
            'new_application': count_new_application(),
            'application': buf_application,
            'media_url': medial_url,
        }
        return render(request, 'feedback/owner/owner_application.html', context)

    def post(self, request, *args, **kwargs):
        models.Application.objects.get(id=kwargs.get('application_id')).delete()
        return HttpResponseRedirect(reverse('cabinet'))


class Owner(View):

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):

        activ_page = 1

        if kwargs.get('active_page') is not None:
            activ_page = int(kwargs.get('active_page'))

        all_applications = models.Application.objects.all().order_by('viewed', '-date')

        list_page = add_list_page(ROWS_PAGE)

        #Добавить внесение активной страницы
        context = {
            'applications': all_applications[int((activ_page-1)*ROWS_PAGE):int(activ_page*ROWS_PAGE-1)],
            'new_application': count_new_application(),
            'count_page': list_page,
            'activ_page': activ_page,
            'max_page':list_page[-1]
        }
        return render(request, 'feedback/owner/owner_base.html', context)

    def post(self):
        pass


class OwnerStatistic(View):

    def get(self, request):
        all_session = Session.objects.filter(session_data__in=["2011-01-01", "2018-02-15"])

        context = {
            'new_application': count_new_application(),
            'all_session': len(all_session)
        }
        return render(request, 'feedback/owner/owner_statistic.html', context)


class Contacts(View):

    def get(self, request):
        return render(request, 'feedback/contacts.html')


class About(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'feedback/about.html')


class Authentication(View):

    def get(self, request, *args, **kwargs):
        urls = request.path_info
        return render(request, 'feedback/login.html')

    def post(self, request, *args, **kwargs):
        user = auth.authenticate(username=request.POST.get('username'),
                                 password=request.POST.get('password'))
        if user is not None and user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('cabinet'))
        else:
            return render(request, 'feedback/login.html')


class Logout(View):

    def get(self, request,  *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('base'))
