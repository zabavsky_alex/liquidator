from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

from feedback.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', Base.as_view(), name='base'),

    url(r'^cabinet/$', Owner.as_view(), name='cabinet'),
    url(r'^cabinet/(?P<active_page>[0-9]+)/$', Owner.as_view(), name='cabinet_acive_page'),
    url(r'^cabinet/application/(?P<application_id>[0-9]+)/$', OneApplication.as_view(), name='one_application'),
    url(r'^cabinet/statistic/$', OwnerStatistic.as_view(), name='cabinet_statistic'),

    url(r'^contacts/$', Contacts.as_view(), name='contacts'),
    url(r'^about/$', About.as_view(), name='about'),

    url(r'^accounts/login/$', Authentication.as_view(), name='login'),
    url(r'^accounts/logout/$', Logout.as_view(), name='logout'),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
