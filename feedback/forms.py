# -*- coding: utf-8 -*-

from datetime import datetime
from django import forms
from django.forms import widgets

import models


CHOICES_BODY_CAR = ((u'Седан', 'Седан'), (u'Универсал', 'Универсал'), (u'Хэтчбек 3 дв.', 'Хэтчбек 3 дв.'),
                    (u'Хэтчбек 5 дв', 'Хэтчбек 5 дв.'), (u'Купе', 'Купе'), (u'Кабриолет', 'Кабриолет'),
                    (u'Внедорожник 3 дв.', 'Внедорожник 3 дв.'), (u'Внедорожник 5 дв.', 'Внедорожник 5 дв.'),
                    (u'Пикап', 'Пикап'), (u'Другой', 'Другой'))
CHOICES_ENGINE_CAR = ((u'Бензин', 'Бензин',), (u'Дизель', 'Дизель',), (u'Газ-Бензин', 'Газ-Бензин'),
                      (u'Гибрид', 'Гибрид'), (u'Электро', 'Электро'))
CHOICES_GEAR_CAR = ((u'Механика', 'Механика',), (u'Автомат', 'Автомат',))


class ApplicationForm(forms.ModelForm):

    class Meta:
        model = models.Application
        fields = ['comment']

    def __init__(self, *args, **kwargs):
        super(ApplicationForm, self).__init__(*args, **kwargs)
        add_input_class(self)

        add_widget_atr(self, 'comment', 'rows', 3)
        self.fields['comment'].required = False
        add_widget_atr(self, 'comment', 'placeholder', u'Расскажите о состоянии своего автомобиля')


class PeopleForm(forms.ModelForm):

    class Meta:
        model = models.People
        fields = ['phone', 'first_name']

    def __init__(self, *args, **kwargs):
        super(PeopleForm, self).__init__(*args, **kwargs)
        add_input_class(self)

        add_widget_atr(self, 'phone', 'type', 'tel')
        add_widget_atr(self, 'phone', 'placeholder', '+375 (xx) xxx xx xx')
        add_widget_atr(self, 'phone', 'pattern',
                       '^(\+375)(\d{2})(\d{3})(\d{2})(\d{2})$')


class CarForm(forms.ModelForm):

    class Meta:
        model = models.Car
        fields = ['model', 'mark', 'body', 'engine', 'price', 'gear', 'fuel', 'age']

    def __init__(self, *args, **kwargs):
        super(CarForm, self).__init__(*args, **kwargs)

        add_widget_atr(self, 'mark', 'placeholder', 'Audi')

        add_widget_atr(self, 'model', 'placeholder', 'A4')

        add_widget_atr(self, 'age', 'min', 1950)
        add_widget_atr(self, 'age', 'max', int(datetime.now().year))

        self.fields['fuel'].widget = widgets.Select(choices=CHOICES_ENGINE_CAR)

        add_widget_atr(self, 'engine', 'min', 0)
        add_widget_atr(self, 'engine', 'max', 15000)
        add_widget_atr(self, 'engine', 'placeholder', 1800)

        add_widget_atr(self, 'price', 'min', 0)

        self.fields['body'].widget = widgets.Select(choices=CHOICES_BODY_CAR)

        self.fields['gear'].widget = widgets.Select(choices=CHOICES_GEAR_CAR)

        add_input_class(self)


class CarMediaForm(forms.ModelForm):

    class Meta:
        model = models.CarMedia
        fields = ['file']

    def __init__(self, *args, **kwargs):
        super(CarMediaForm, self).__init__(*args, **kwargs)
        #несколько файлов
        #add_widget_atr(self, 'file', 'multiple', 'True')


def add_input_class(self):
    for field in self.fields:
        self.fields[field].widget.attrs['class'] = 'form-control'


def add_widget_atr(self, row, param, set_param):
    self.fields[row].widget.attrs[param] = set_param


