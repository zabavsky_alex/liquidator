import models


def add_list_page(rows_page):
    all_applications = models.Application.objects.all().order_by('viewed', '-date')
    count_page = len(all_applications) / rows_page
    if count_page % 2 is not None:
        count_page += 1

    list_page = list()
    for page in range(1, count_page + 1):
        list_page.append(page)

    return list_page


def count_new_application():
    return len(models.Application.objects.filter(viewed=False))


def add_nokey_session(request):
    if not request.session.session_key:
        request.session.create()
    return request
