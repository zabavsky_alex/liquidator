# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

import datetime


class People(models.Model):
    first_name = models.CharField(max_length=100)
    phone = PhoneNumberField()

    def get_phone(self):
        str_phone = str(self.phone)
        phone = str(str_phone[0:4] + ' (' + str_phone[4:6] + ') ' + str_phone[6:9] + ' ' +
                    str_phone[9:11] + ' ' + str_phone[11:13])
        return phone


class Car(models.Model):
    people = models.ForeignKey(People)
    mark = models.CharField(max_length=50, null=True)
    model = models.CharField(max_length=50, null=True)
    age = models.IntegerField(null=True)
    engine = models.IntegerField(null=True)
    fuel = models.CharField(max_length=50, null=True)
    gear = models.CharField(max_length=50, null=True)
    body = models.CharField(max_length=50, null=True)
    price = models.IntegerField(null=True)


class CarMedia(models.Model):
    car = models.ForeignKey(Car)
    file = models.ImageField(upload_to="images/", null=True, blank=True)


class Application(models.Model):
    people = models.ForeignKey(People, on_delete=models.CASCADE)
    car = models.OneToOneField(Car,  on_delete=models.CASCADE)
    date = models.DateTimeField()
    comment = models.TextField(max_length=200, null=True)
    viewed = models.BooleanField(default=False)

    def get_date(self):
        str_date = ''
        if datetime.date.today() == self.date.date():
            str_date = "сегодня " + str(self.date.hour) + ":" + str(self.date.minute)
        else:
            str_date = str(self.date.date())
        return str_date

