# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
import models


admin.site.register(models.People)
admin.site.register(models.Car)
admin.site.register(models.Application)
admin.site.register(models.CarMedia)
